name: "kselftests mm"
maintainers:
  - name: Memory Management
    email: mm-qe@redhat.com
    gitlab: mm-qe
sets:
  - kself
location: kselftests
origin: kernel_public_tests
universal_id: redhat_vm_self_tests
host_types: kselftests_vm
trigger_sources:
  - tools/testings/selftests/mm/run_vmtests\.sh
  - arch/x86/mm/.*
  - arch/s390/mm/.*
  - arch/arm64/mm/.*
  - arch/powerpc/mm.*
  - mm/internal\.h
  - mm/vma.*
  - mm/Makefile
  - mm/Kconfig
  - mm/memory\.c
waived: true
enabled:
  trees:
    or:
      - rhel9.*|c9s
      - rhel10.*|c10s
    not:
      or:
        - rhel9[0-4].*
  components:
    - not: rt
environment:
  DELIVERED_TESTS: "1"
  TEST_ITEMS: mm
cases:
  mmap:
    name: "mmap"
    environment:
      VM_SELFTEST_ITEMS: "mmap"
    target_sources:
      - tools/testings/selftests/mm/map_fixed_noreplace\.c
      - tools/testings/selftests/mm/map_populate\.c
      - mm/mmap.*
      - mm/rmap\.c
      - mm/mremap\.c
  compaction:
    name: "compaction"
    environment:
      VM_SELFTEST_ITEMS: "compaction"
    target_sources:
      - tools/testings/selftests/mm/compaction_test\.c
      - mm/compaction\.c
      - mm/workingset\.c
      - mm/swap\.c
      - mm/readahead\.c
      - mm/page_counter\.c
      - mm/mmzone\.c
  mlock:
    name: "mlock"
    environment:
      VM_SELFTEST_ITEMS: "mlock"
    target_sources:
      - mm/mlock\.c
      - mm/rmap\.c
      - tools/testings/selftests/mm/mlock.*
  mremap:
    name: "mremap"
    environment:
      VM_SELFTEST_ITEMS: "mremap"
    target_sources:
      - mm/mmap.*
      - mm/rmap\.c
      - mm/mremap\.c
  vmalloc:
    name: "vmalloc"
    environment:
      VM_SELFTEST_ITEMS: "vmalloc"
    target_sources:
      - mm/vmalloc\.c
      - tools/testings/selftests/mm/test_vmalloc\.sh
      - lib/test_vmalloc\.c
      - lib/mm/kasan/.*
  soft_dirty:
    name: "soft_dirty"
    environment:
      VM_SELFTEST_ITEMS: "soft_dirty"
    target_sources:
      - tools/testing/selftests/mm/soft-dirty\.c
      - mm/pgtable-generic\.c
  cow:
    name: "cow"
    environment:
      VM_SELFTEST_ITEMS: "cow"
    target_sources:
      - tools/testing/selftests/mm/cow\.c
      - mm/gup\.c
      - mm/hugetlb\.c
      - mm/huge_memory\.c
  hugetlb:
    name: "hugetlb"
    environment:
      VM_SELFTEST_ITEMS: "hugetlb"
    target_sources:
      - tools/testing/selftests/mm/.*huge.*
      - mm/hugetlb.*
      - mm/huge_memory\.c
      - mm/khugepaged\.c
      - mm/highmem\.c
  migration:
    name: "migration"
    host_types: kselftests_vm_numa
    environment:
      VM_SELFTEST_ITEMS: "migration"
    enabled:
      not:
        arches:
          - s390x
    target_sources:
      - tools/testing/selftests/mm/migration\.c
      - mm/memremap\.c
      - mm/migrate\.c
      - mm/userfaultfd\.c
  gup:
    name: "gup"
    environment:
      VM_SELFTEST_ITEMS: "gup_test"
    enabled:
      components: debugbuild
    target_sources:
      - tools/testing/selftests/mm/gup.*
      - mm/gup.*
  userfaultfd:
    name: "userfaultfd"
    environment:
      VM_SELFTEST_ITEMS: "userfaultfd"
    target_sources:
      - tools/testing/selftests/mm/uffd.*
      - fs/userfaultfd\.c
      - mm/userfaultfd\.c
  thp:
    name: "thp"
    environment:
      VM_SELFTEST_ITEMS: "thp"
    enabled:
      components:
        - not: 64kpages
    target_sources:
      - tools/testing/selftests/mm/.*huge.*
      - mm/hugetlb.*
      - mm/huge_memory\.c
      - mm/khugepaged\.c
      - mm/highmem\.c
  hmm:
    name: "hmm"
    environment:
      VM_SELFTEST_ITEMS: "hmm"
    target_sources:
      - tools/testing/selftests/mm/hmm-tests\.c
      - mm/hmm\.c
      - lib/test_hmm\.c
  madv_populate:
    name: "madv_populate"
    environment:
      VM_SELFTEST_ITEMS: "madv_populate"
    target_sources:
      - tools/testings/selftests/mm/madv_populate\.c
      - mm/mmap.*
      - mm/rmap\.c
      - mm/mremap\.c
  memfd_secret:
    name: "memfd_secret"
    environment:
      VM_SELFTEST_ITEMS: "memfd_secret"
    target_sources:
      - tools/testing/selftests/mm/memfd_secret\.c
      - mm/memfd\.c
  process_mrelease:
    name: "process_mrelease"
    environment:
      VM_SELFTEST_ITEMS: "process_mrelease"
    target_sources:
      - tools/testing/selftests/mm/mrelease_test\.c
  ksm:
    name: "ksm"
    environment:
      VM_SELFTEST_ITEMS: "ksm"
    target_sources:
      - tools/testing/selftests/mm/ksm.*
      - mm/ksm\.c
      - mm/usercopy\.c
      - mm/mmu_gather\.c
      - mm/readahead\.c
      - mm/rmap\.c
      - mm/mmap\.c
      - mm/mremap\.c
  ksm_numa:
    name: "ksm_numa"
    environment:
      VM_SELFTEST_ITEMS: "ksm_numa"
    host_types: kselftests_vm_numa
    enabled:
      components:
        - not: 64kpages
      arches:
        not:
          - s390x
    target_sources:
      - tools/testing/selftests/mm/ksm.*
      - mm/ksm\.c
      - mm/usercopy\.c
      - mm/mmu_gather\.c
      - mm/readahead\.c
      - mm/rmap\.c
      - mm/mmap\.c
      - mm/mremap\.c
  pkey:
    name: "pkey"
    environment:
      VM_SELFTEST_ITEMS: "pkey"
    target_sources:
      - tools/testing/selftests/mm/pkey.*
      - tools/testing/selftests/mm/protection.*
      - mm/mprotect\.c
      - mm/mmap\.c
  hugevm:
    name: "hugevm"
    enabled: false
    environment:
      VM_SELFTEST_ITEMS: "hugevm"
    target_sources:
      - tools/testing/selftests/mm/virtual_address_range\.c
      - tools/testing/selftests/mm/va_high_addr_switch.*
      - mm/pgtable-generic\.c
      - mm/highmem\.c
  mdwe:
    name: "mdwe"
    environment:
      VM_SELFTEST_ITEMS: "mdwe"
    enabled: false
  mkdirty:
    name: "mkdirty"
    environment:
      VM_SELFTEST_ITEMS: "mkdirty"
